import sys
import math
import PIL
import multiprocessing as mp
from multiprocessing import Pool
from functools import partial
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
import cProfile

#Implementar la low cost approximacion
#Poder agregar los sprites faltantes
#Implementar un paso adaptativo para sprites que son mas grandes
CORES = 6

WHITE = (255,255,255) 
BLACK = (0,0,0)

FONT_SIZE  = 14
TSIZE      = 17
SPRITESIZE = 16

OVERWORLD   = 0
UNDERGROUND = 1
CASTLE      = 2
UNDERWATER  = 3
GHOST       = 4
AIRSHIP     = 5

WORLDS = [
           'GROUND_DAY'
          ,'SKY_DAY'
          ,'GROUND_NIGHT'
          ,'SKY_NIGHT'
          ,'UNDERGROUND_DAY'
          ,'FOREST_DAY'     
          ,'UNDERGROUND_NIGHT'
          ,'FOREST_NIGHT'
          ,'UNDERWATER_DAY'
          ,'GHOST_DAY'
          ,'UNDERWATER_NIGHT'
          ,'GHOST_NIGHT'
          ,'DESERT_DAY'
          ,'AIRSHIP_DAY'
          ,'DESERT_NIGHT'
          ,'AIRSHIP_NIGHT'
          ,'SNOW_DAY'
          ,'CASTLE_DAY'
          ,'SNOW_NIGHT'
          ,'CASTLE_NIGHT'
         ]


#Create a dictionary with the average color for each tile in the line of tiles
def get_tiles(img, tiles_number, world_type, width = TSIZE, height = TSIZE):
    tiles = {}
    tile_y = world_type * height
    for w in range(tiles_number):
        tile_x = w * width
        tile = img.crop((tile_x + 1, tile_y + 1, tile_x + width, tile_y + height))
        tiles[tuple(average_image(tile))] = tile
    return tiles

#Generate a dictionary for every world type and each one having a dictionary of tiles
def generate_tiles_dict(img,worlds_number,width = TSIZE,height = TSIZE):
    tiles = {}
    (img_width,img_height) = img.size
    world_tiles_number = int(img_width/width) #If there is a single vertical line at the end we can ignore it.
    for world in range(worlds_number):
        tiles[world] = get_tiles(img, world_tiles_number, world, width, height) 
    return tiles

#Given an image return the average of all the colors in the image.
def average_image(im):
    data = list(im.getdata())
    size = len(data)
    return [sum(x)/size for x in zip(*data)]

#Color distance function
#found here https://www.compuphase.com/cmetric.htm
def color_distance(color1,color2):
    dist = 0
    r = (color1[0]+color2[0])/2
    if len(color1) < 4 or len(color2) < 4:
        dist = math.sqrt((2+r/256)*(color1[0]-color2[0])**2+4*(color1[1]-color2[1])**2+(2+(255-r)/256)*(color1[2]-color2[2])**2)
    else:
        dist = math.sqrt((2+r/256)*(color1[0]-color2[0])**2+4*(color1[1]-color2[1])**2+(2+(255-r)/256)*(color1[2]-color2[2])**2+(color1[3]-color2[3])**2)
    return dist

#Compare an image against the images in a dictionary to get the closest one. Really resource intensive
def get_closest_image(imagesegment,dictionary):
    distance = 4000000.0
    bestvalue = None
    for key, val in dictionary.items():
        imagesegment_data = list(imagesegment.getdata())
        val_data = list(val.getdata())
        if not False:
            newdist = sum([color_distance(color1,color2) for color1,color2 in zip(imagesegment_data,val_data)])
            if newdist < distance:
                distance = newdist
                bestvalue = val
    return bestvalue

#Given a pixel return the image that gets the closest to the pixel given its average value
def get_closest_color(pixel,dictionary):
    distance = 1000.0
    bestchar = None
    for key, val in dictionary.items():
        if not False:
            newdist = math.sqrt(sum([(x-y)**2 for x,y in zip(pixel,key)]))
            if  newdist < distance:
                distance = newdist
                bestchar = val
    return bestchar

#For a dictionary of colors and averages it finds the closest image using a color distance function
def get_closest_color_weighted(pixel,dictionary):
    distance = 1000.0
    color = None
    for key, val in dictionary.items():
        newdist = color_distance(pixel,key)
        if  newdist < distance:
            distance = newdist
            color = val
    return color

#Takes an image and applies the nth world type to it and saves it to a file.
#This finds the closest image, not the closest average.
def mario_modified(image_name, tiles_name, scale, world_type, number_worlds, ssize = SPRITESIZE):
    print('------------------------------------')
    img       = Image.open(image_name)
    tiles_img = Image.open(tiles_name)
    usedtiles = {}    
    tiles = generate_tiles_dict(tiles_img, number_worlds,ssize + 1, ssize +1) #adding 1 to ssize to account for the line separator in the tiles
    if scale:
        img = img.resize(scale)
    (width,height) = img.size
    canvas = Image.new("RGBA",(int(width/ssize) * ssize, int(height/ssize) * ssize), BLACK)
    for h in range(int(height/ssize)):
        for w in range(int(width/ssize)):
            subimage = img.crop((w*ssize,h*ssize,(w+1)*ssize,(h+1)*ssize))
            average = tuple(average_image(subimage))
            if average in usedtiles:
                canvas.paste(usedtiles[average],(w*ssize,h*ssize),usedtiles[average])
            else: 
                current_tile = get_closest_image(subimage, tiles[world_type])
                usedtiles[average] = current_tile    
                canvas.paste(current_tile,(w*ssize,h*ssize),current_tile)
        print(str(len(usedtiles))+"_"+str(h)) 
    canvas.save('modified_' + image_name.rsplit('.',1)[0] + '_' + WORLDS[world_type] + '_' + tiles_name.rsplit('.',1)[0] + '_' + str(ssize)  + ".png")

#Takes an image and applies the nth world type to it and saves it to a file.
#Single core version
def mario(image_name, tiles_name, scale, world_type, number_worlds, ssize = SPRITESIZE):
    img       = Image.open(image_name)
    tiles_img = Image.open(tiles_name)
    pixelmapping = {}    
    tiles = generate_tiles_dict(tiles_img, number_worlds,ssize + 1, ssize +1) #adding 1 to ssize to account for the line separator in the tiles
    if scale:
        img = img.resize(scale)
    (width,height) = img.size
    canvas = Image.new("RGBA",(width * ssize, height * ssize), BLACK)
    for h in range(height):
        for w in range(width):
            pixel = img.getpixel((w,h))
            if tuple(img.getpixel((w,h))) not in pixelmapping:
                pixelmapping[pixel] = get_closest_color_weighted(img.getpixel((w,h)),tiles[world_type])
            canvas.paste(pixelmapping[pixel],(w*ssize,h*ssize),pixelmapping[pixel])
    canvas.save(image_name.rsplit('.',1)[0] + '_' + WORLDS[world_type] + '_' + tiles_name.rsplit('.',1)[0] + '_' + str(ssize)  + ".png")

#Same as the mario function but this is the paralell version.
def mario_parallel(image_name, tiles_name, scale, world_type, number_worlds, ssize = SPRITESIZE):
    img       = Image.open(image_name)
    tiles_img = Image.open(tiles_name)
    pixelmapping = {}    
    tiles = generate_tiles_dict(tiles_img, number_worlds,ssize + 1, ssize +1) #adding 1 to ssize to account for the line separator in the tiles
    if scale:
        img = img.resize(scale)
    (width,height) = img.size
    canvas = Image.new("RGBA",(width * ssize, height * ssize), BLACK)
    #get the pixels in the image
    img_dataset = list(img.getdata())
    #remove duplicates
    unique_colors = set(img_dataset)
    #convert back to a list before sending to the multiple processes.
    img_dataset = list(unique_colors)
    #Create the pool of processes and calculate the best match for each
    p = Pool(processes=CORES)
    get_closest_color_weighted_partial = partial(get_closest_color_weighted,dictionary=tiles[world_type])
    result = p.map(get_closest_color_weighted_partial,img_dataset,chunksize=int(len(img_dataset)/CORES))
    #Create a dictionary with the sprites and colors
    color_dict = dict(zip(img_dataset,result))

    for h in range(height):
        for w in range(width):
            current_tile = color_dict[img.getpixel((w,h))]
            canvas.paste(current_tile,(w*ssize,h*ssize),current_tile)
    canvas.save(image_name.rsplit('.',1)[0] + '_' + WORLDS[world_type] + '_' + tiles_name.rsplit('.',1)[0] + '_' + str(ssize)  + ".png")

def mario_set_modified(image_name,tiles_name,scale,number_worlds = 20, ssize = SPRITESIZE):
    for world_type in range(number_worlds):
        print('Converting using the ' + WORLDS[world_type] + ' tileset.')
        mario_modified(image_name, tiles_name, scale, world_type, number_worlds, ssize)

#Runs the mario function for as many worlds as specified. Ideally it does it for all the worlds in the tiles available
def mario_set(image_name,tiles_name,scale,number_worlds = 20, ssize = SPRITESIZE):
    for world_type in range(number_worlds):
        print('Converting using the ' + WORLDS[world_type] + ' tileset.')
        mario(image_name, tiles_name, scale, world_type, number_worlds, ssize)

#Same as mario set but uses the parallel version
def mario_set_parallel(image_name,tiles_name,scale,number_world = 'All', ssize = SPRITESIZE):
    if number_world == 'All':
        for world_type in range(len(WORLDS)):
            print('Converting using the ' + WORLDS[world_type] + ' tileset.')
            mario_parallel(image_name, tiles_name, scale, world_type, len(WORLDS), ssize)
    else:
        print('Converting using the ' + WORLDS[number_world] + ' tileset.')
        mario_parallel(image_name, tiles_name, scale, number_world, len(WORLDS), ssize)

if len(sys.argv) in [5,6]:
    print('Image Name: ' + str(sys.argv[1]))
    print('Tiles Name: ' + str(sys.argv[2]))
    print('Scale(Tuple): ' + str((int(sys.argv[3]),int(sys.argv[4]))))
    if len(sys.argv) == 6:
        print('World Type: ' + str(WORLDS[int(sys.argv[5])]))
        mario_set_parallel(str(sys.argv[1]),str(sys.argv[2]),(int(sys.argv[3]),int(sys.argv[4])),int(sys.argv[5]))
    else:
        mario_set_parallel(str(sys.argv[1]),str(sys.argv[2]),(int(sys.argv[3]),int(sys.argv[4])))
else: 
    print('This program needs 4 or 5 parameters.')
    print('image_name tiles_file width height [world_type]')
    print('width and height indicate the resized dimensions before converting the image')
    print('world type is an optional value from 0 to 19. If no world type is used all the styles will be generated in sequence.')
    for idx, value in enumerate(WORLDS):
        print('World type {} Meaning {}'.format(idx,value))
    print('Examples:')
    print('python image2mario.py photo.png tiles.png 800 600')
    print('python image2mario.py photo.png tiles.png 800 600 5')
