#!/usr/bin/python3
import math
import PIL
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

WHITE = (255,255,255) 
BLACK = (0,0,0)

TSIZE   = 17
MAPSIZEWIDTH = 16
WORLDPADDING = 2
HEADERPADDING = 11
DAYPADDING = 3
MAPSIZEHEIGHT = 43
TILES = ['0_0','0_1','0_2','0_4','0_5','0_6','0_7','0_9','0_14','0_15'
        ,'1_14','1_15'
        ,'2_4'
        ,'3_1','3_4','3_8','3_10'
        ,'4_0','4_2','4_4','4_6','4_8','4_10','4_14'
        ,'5_6'
        ,'6_6'
        ,'7_8','7_9','7_14'
        ,'11_14'
        ,'12_4','12_6'
        ,'13_1','13_4'
        ,'15_8']
#Create a list that contains a two character representation of each map type location
def create_world_name(sizex, sizey):
    WORLDS = []
    for x in range(sizex):
        for y in range(sizey):
            WORLDS.append(str(y) + str(x))
    return WORLDS

#given a map obtain a list of all the tiles in the list of valid TILES.
def get_tiles(img, map_x, map_y, time, width = TSIZE, height = TSIZE):
    tiles = []
    for w in range(MAPSIZEWIDTH):
        for h in range(MAPSIZEHEIGHT):
            tile_x = map_x * WORLDPADDING + map_x * width * MAPSIZEWIDTH + w * width
            tile_y = (int(map_y/2) + map_y + 1) * HEADERPADDING + HEADERPADDING + map_y * WORLDPADDING +int(map_y/2) * DAYPADDING + map_y * height * MAPSIZEHEIGHT + h * height
            if str(h) + '_' + str(w) in TILES or True:
                tile = img.crop((tile_x, tile_y, tile_x + width + 1, tile_y + height + 1))
                tiles.append(tile)
    return tiles

#generate a dictionary for the different map styles with a list of tiles for each
def generate_tiles_dict(img, maps_x, maps_y, width = TSIZE, height = TSIZE):
    tilesdict = {}
    for h in range(maps_y):
        for w in range(maps_x):
            tilesdict[str(h) + str(w)] = get_tiles(img,w,h,width,height) 
    return tilesdict

#Create an image, one line per map style with all the images, it preserves transparency
def create_tileset(tilesdict, name, worlds, tilesize=TSIZE):
    width  = len(tilesdict[worlds[0]])
    height = len(worlds)
    canvas = Image.new("RGBA",(width * tilesize, height * tilesize + 1), WHITE)
    
    for wpos,worldtype in enumerate(worlds):
        for tpos,tile in enumerate(tilesdict[worldtype]):
            canvas.paste(tile,(tpos * tilesize, wpos * tilesize))
    canvas.save(name.rsplit('.',1)[0] + '_tiles.png')

#Call the previous functions so that the user only needs to call one fuction
def generate_style_tileset(filename, maps_x, maps_y,tilesize = TSIZE):
    tileimg = Image.open(filename)
    worlds = create_world_name(maps_x, maps_y)
    tiledict = generate_tiles_dict(tileimg,maps_x,maps_y,tilesize,tilesize)
    create_tileset(tiledict, filename,worlds,tilesize)



generate_style_tileset("mario1.png",5,4)
generate_style_tileset("mario3.png",5,4)
generate_style_tileset("mariow.png",5,4)
#generate_style_tileset("mariou.png",1,6,65)

